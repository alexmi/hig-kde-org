Emblem Icons
============
Emblems icons are used to badge other icons and convey some type of status that
pertains to them. They come in three sizes: 8px, 16px, and 22px, and always use
the `colorful style <index.html#colorful-icon-style>`__. However, their color 
palette is limited to that of the
monochromatic style. Unlike other icons, they are drawn with zero margins and
touch the edges of the canvas.

Emblem icons always have a colored background shape and a monochrome foreground
symbol. Because of the extremely limited space available, it is critical that
the foreground symbol be aligned to the pixel grid:

.. figure:: /img/Breeze-icon-design-emblem.png
   :alt: Pixel-perfect emblem icon

   Pixel-perfect emblem icon with zero margins

16px and 22px Emblems get a 60% opacity outline to ensure adequate contrast
against whatever icon they are drawn on top of:

.. figure:: /img/Breeze-icon-design-emblem-16px.png
   :alt: 16px emblem icons

   16px emblems

.. figure:: /img/Breeze-icon-design-emblem-22px.png
   :alt: 22px emblem icons

   22px emblems

8px emblems don't have an outline, because there simply is not enough room:

.. figure:: /img/Breeze-icon-design-emblem-8px.png
   :alt: 8px emblem icons

   8px emblems
